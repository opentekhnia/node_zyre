/**
 * Module dependencies.
 */

var EventEmitter = require('events').EventEmitter
  , zyre = require('bindings')('zyre.node')
  , util = require('util');

/**
 * Expose bindings as the module.
 */

exports = module.exports = zyre;

/**
 * Expose zmq version.
 */

exports.version = zyre.zmqVersion();

/**
 * Events
 */
var events = exports.events = {
    1: "ENTER"    // ZYRE_EVENT_ENTER
  , 2: "JOIN"     // ZYRE_EVENT_JOIN
  , 3: "LEAVE"    // ZYRE_EVENT_LEAVE
  , 4: "EXIT"     // ZYRE_EVENT_EXIT
  , 5: "WHISPER"  // ZYRE_EVENT_WHISPER
  , 6: "SHOUT"    // ZYRE_EVENT_SHOUT
}

/**
 * Create a new node.
 *
 * @constructor
 * @api public
 */

var Node =
exports.Node = function (name) {
  EventEmitter.call(this);
  this._zyre = new zyre.NodeBinding(name);
  //this._poller = new zyre.PollerBinding(this._zyre);
};


/**
 * Inherit from `EventEmitter.prototype`.
 */

util.inherits(Node, EventEmitter);


/**
 * Set header as a formated string with `format` for `header`.
 *
 * @param {String} format
 * @param {String} header
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.setHeader = function(name, header){
  this._zyre.setHeader(name, header);
  return this;
};


/**
 * Get node `uuid`.
 *
 * @return {String}
 * @api public
 */

Node.prototype.__defineGetter__('uuid', function() {
  return this._zyre.uuid;
});


/**
 * Set node to verbose.
 *
 * @param {Boolean} value
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.__defineSetter__('verbose', function(value) {
  this._zyre.verbose = value;
  return this;
});


/**
 * Returns node verbose.
 *
 * @return {Boolean}
 * @api public
 */

Node.prototype.__defineGetter__('verbose', function() {
  return this._zyre.verbose;
});


/**
 * Set node interval.
 *
 * @param {Integer} value
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.__defineSetter__('interval', function(value) {
  this._zyre.interval = value;
  return this;
});


/**
 * Get node interval.
 *
 * @return {Integer}
 * @api public
 */

Node.prototype.__defineGetter__('interval', function() {
  return this._zyre.interval;
});


/**
 * Set node port.
 *
 * @param {Integer} value
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.__defineSetter__('port', function(value) {
  this._zyre.port = value;
  return this;
});


/**
 * Get node port.
 *
 * @return {Integer}
 * @api public
 */

Node.prototype.__defineGetter__('port', function() {
  return this._zyre.port;
});


/**
 * Destroy node.
 *
 * @return {Node} for chaining (but node destroyed)
 * @api public
 */

Node.prototype.destroy = function() {
  this._zyre.destroy();
  return this;
};


/**
 * Start node.
 *
 * @return {Node} for chaining 
 * @api public
 */

Node.prototype.start = function() {
  return this._zyre.start();;
};


/**
 * Stop node.
 *
 * @return {Node} for chaining 
 * @api public
 */

Node.prototype.stop = function() {
  this._zyre.stop();
  return this;
};


/**
 * Join group.
 *
 * @param {String} group's name
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.join = function(group) {
  this._zyre.join(group);

  return this;
};


/**
 * Leave group.
 *
 * @param {String} group's name
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.leave = function(group) {
  this._zyre.leave(group);

  return this;
};


/**
 * Shouts (see zyre doc).
 *
 * @param {String} string message to shout
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.shouts = function(group, msg) {
  this._zyre.shouts(group, msg);

  return this;
};


/**
 * Whispers (see zyre doc).
 *
 * @param {String} peer to whisper to
 * @param {String} string message to whisper
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.whispers = function(peer, msg) {
  this._zyre.whispers(peer, msg);

  return this;
};


/**
 * Dump (see zyre doc).
 *
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.dump = function() {
  this._zyre.dump();

  return this;
};


/**
 * Test (see zyre doc).
 *
 * @return {Node} for chaining
 * @api public
 */

Node.prototype.test = function(verbose) {
  this._zyre.test(verbose);

  return this;
};


exports.node =
exports.createNode = function(name) {
  var node = new Node(name);
  return node;
};
